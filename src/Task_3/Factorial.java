package Task_3;

public class Factorial {
	public double factorial(double n) {
		if (n < 0)
			return Double.POSITIVE_INFINITY;
		else if (n < 2)
			return 1;
		else
			return n * factorial(n - 1);
	}
}
