package Task_3;
import static org.junit.Assert.*;

import org.junit.Test;
public class FactorialTest {
	@Test
	public void Test() {
		Factorial f = new Factorial();
		
		assertTrue(f.factorial(0) == 1);
		assertTrue(f.factorial(1) == 1);
		assertTrue(f.factorial(2) == 2);
		assertTrue(f.factorial(3) == 6);
		assertTrue(f.factorial(8) == 40320);
		assertTrue(f.factorial(500) == Double.POSITIVE_INFINITY);	
		assertTrue(f.factorial(-1) == Double.POSITIVE_INFINITY);
	}
}
