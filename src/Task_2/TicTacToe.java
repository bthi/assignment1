package Task_2;


public class TicTacToe {
	private char board[][] = new char[3][3];
	private Player p1 = new Player('X');
	private Player p2 = new Player('O');
	private boolean turn = true;
	
	TicTacToe() {
		clearBoard();
	}
	
	public Player getP1() {
		return p1;
	}
	
	public Player getP2() {
		return p2;
	}
	
	public void displayBoard() {
		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 3; y++) {
				System.out.print(board[x][y] + " ");
			}
			System.out.println();
		}
		System.out.println();
	}
	
	public boolean isDraw() {
		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 3; y++) {
				if (board[x][y] == '_')
					return false;
			}
		}
		return (isWinner(p1) == false && isWinner(p2) == false);			
	}
	
	public boolean isWinner(Player p) {
		char piece = p.getPiece();
		if (board[0][0] == piece && board[0][1] == piece && board[0][2] == piece) {
			//System.out.println("Top horizontal");
			return true;
		}
		else if (board[1][0] == piece && board[1][1] == piece && board[1][2] == piece) {
			//System.out.println("Middle horizontal");
			return true;
		}
		else if (board[2][0] == piece && board[2][1] == piece && board[2][2] == piece) {
			//System.out.println("Bottom horizontal");
			return true;
		}
		else if (board[0][0] == piece && board[1][1] == piece && board[2][2] == piece) {
			//System.out.println("Down right diagonal");
			return true;
		}
		else if (board[0][2] == piece && board[1][1] == piece && board[2][0] == piece) {
			//System.out.println("Down left diagonal");
			return true;
		}
		else if (board[0][0] == piece && board[1][0] == piece && board[2][0] == piece) {
			//System.out.println("Left Vertical");
			return true;
		}
		else if (board[0][1] == piece && board[1][1] == piece && board[2][1] == piece) {
			//System.out.println("Middle vertical");
			return true;
		}
		else if (board[0][2] == piece && board[1][2] == piece && board[2][2] == piece) {
			//System.out.println("Right vertical");
			return true;
		}
		return false;
	}
		
	public void clearBoard() {
		turn = true;
		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 3; y++) {
				board[x][y] = '_';
			}
		}
	}
	
	private void insertPiece(int x, int y) {
		if (board[x][y] == '_' && !isWinner(p1) && !isWinner(p2)) {	
			if (turn)
				board[x][y] = p1.getPiece();
			else 
				board[x][y] = p2.getPiece();
			turn = !turn;
		}
		displayBoard();
	}

	public void markTopLeft() {
		insertPiece(0, 0);
	}
	
	public void markTopCenter() {
		insertPiece(0, 1);
	}
	
	public void markTopRight() {
		insertPiece(0, 2);
	}
	
	public void markMiddleLeft() {
		insertPiece(1, 0);
	}
	
	public void markMiddleCenter() {
		insertPiece(1, 1);
	}
	
	public void markMiddleRight() {
		insertPiece(1, 2);
	}
	
	public void markBottomLeft() {
		insertPiece(2, 0);
	}
	
	public void markBottomCenter() {
		insertPiece(2, 1);
	}
	
	public void markBottomRight() {
		insertPiece(2, 2);
	}
}
