package Task_2;

import static org.junit.Assert.*;

import org.junit.Test;

public class TicTacToeTest {
	
	@Test
	public void test() {
		TicTacToe t = new TicTacToe();
			
		t.markBottomCenter();
		t.markBottomRight();
		t.markBottomLeft();
		t.markMiddleCenter();
		t.markMiddleLeft();
		t.markMiddleRight();
		t.markTopCenter();
		t.markTopLeft();
		t.markTopRight();
		t.markMiddleLeft();
		
		assertFalse(t.isDraw());
		assertFalse(t.isWinner(t.getP1()));
		assertTrue(t.isWinner(t.getP2()));
		
		t.clearBoard();
		
		t.markTopLeft();
		t.markMiddleCenter();
		t.markTopRight();
		t.markTopCenter();	
		t.markMiddleLeft();		
		t.markMiddleRight();
		t.markBottomCenter();
		t.markBottomLeft();
		t.markBottomRight();
		
		assertFalse(t.isWinner(t.getP1()));
		assertFalse(t.isWinner(t.getP2()));
		assertTrue(t.isDraw());
	}

}
