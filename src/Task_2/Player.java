package Task_2;

public class Player {
	private char piece;
	
	Player(char piece) {
		this.piece = piece;
	}
	
	public char getPiece() {
		return piece;
	}		
}
