package Task_1;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class FibonacciTest {
	@Test
	public void test () {
		Fibonacci f = new Fibonacci();
		
		assertTrue(f.fibonacci(1) == 0);
		assertTrue(f.fibonacci(2) == 1);
		assertTrue(f.fibonacci(3) == 1);
		assertTrue(f.fibonacci(4) == 2);
		assertTrue(f.fibonacci(12) == 89);
		assertTrue(f.fibonacci(20) == 4181);
		assertTrue(f.fibonacci(29) == 317811);
		assertTrue(f.fibonacci(-1) == null);
	}
}
