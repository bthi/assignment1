package Task_1;

public class Fibonacci {
	public Integer fibonacci(int n) {
		if (n <= 0)
			return null;
		else if (n == 1) 
			return 0;
		else if (n <= 3)
			return 1;
		else 
			return fibonacci(n - 1) + fibonacci(n - 2);
	}
 }
