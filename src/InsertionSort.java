
public class InsertionSort {

	public Integer[] sort(Integer[] unsortedList, Integer size) {
		Integer numberToInsert;
		Integer hole;
		
		for (Integer i = 1 ; i < size; i ++) {
			numberToInsert = unsortedList[i];
			hole = i;
			while (hole > 0 && unsortedList[hole - 1] > numberToInsert) {
				unsortedList[hole] = unsortedList[hole - 1];
				hole--;
			}
			unsortedList[hole] = numberToInsert;
		}
		return unsortedList;
	}

}
