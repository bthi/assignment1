import static org.junit.Assert.*;

import org.junit.Test;


public class test {
	@Test
	public void testBasicInsertionSort() {
		Integer[] unsortedList = {10,9,8,7,6,5,4,3,2,1};
		InsertionSort insertionSort = new InsertionSort();
		unsortedList = insertionSort.sort(unsortedList, 10);
		
		for (Integer i = 0; i < unsortedList.length; i++)
			System.out.println(unsortedList[i]);
		
		for (Integer i = 0; i < 9; i++) {
			assertTrue(unsortedList[i] < unsortedList[i+1]);
		}
	}
}
